Rails.application.routes.draw do
  resources :irc do
    get :users_participation, on: :collection
  end

end